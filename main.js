import Vue from 'vue'
import App from './App'
import config from '@/config.js'
import store from '@/store'

Vue.config.productionTip = false
App.mpType = 'app'
Vue.prototype.$ossDomain = config.OSS_DOMAIN;

let vuexStore = require("@/store/$u.mixin.js");
Vue.mixin(vuexStore);

// 引入uView
import uView from 'uview-ui'
Vue.use(uView);

// #ifdef H5
import wechat from '@/utils/wxH5/jssdk.js';
if (wechat.isWechat()) {
	Vue.prototype.$wechat = wechat;
}
// #endif

// #ifndef H5
// 引入uView对小程序分享的mixin封装
let mpShare = require('uview-ui/libs/mixin/mpShare.js');
Vue.mixin(mpShare);
// #endif

const app = new Vue({
	store,
	...App
})

// http拦截器
import httpInterceptor from '@/utils/http.interceptor.js'
Vue.use(httpInterceptor, app)

// http接口API集中管理
import httpApi from '@/utils/http.api.js'
Vue.use(httpApi, app)

app.$mount()
